package org.aliquam.session.service.service;

import org.aliquam.session.api.model.CreateSessionRequest;
import org.aliquam.session.api.model.Session;
import org.aliquam.session.service.model.ContractModel;
import org.aliquam.session.service.model.SessionModel;
import org.aliquam.session.service.repository.ContractRepository;
import org.aliquam.session.service.repository.SessionRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Most of the tests are located in alq-service-integration-tests!
 * Below tests are here because it'd be hard to test time-related things from alq-service-integration-tests.
 */
@SpringBootTest
class SessionServiceTest {

    @MockBean
    private ContractRepository contractRepository;

    @MockBean
    private SessionRepository sessionRepository;

    @Autowired
    private SessionService sessionService;

    @Test
    void createdSessionExpiresAfterFewDays() {
        LocalDateTime now = LocalDateTime.of(2020, 6, 1, 12, 0, 0);
        UUID CONTRACT_UUID = UUID.fromString("00000000-4176-4c73-b121-8d8fc13a4400");
        CreateSessionRequest request = new CreateSessionRequest(CONTRACT_UUID, "password", null);

        when(contractRepository.findById(CONTRACT_UUID))
                .thenReturn(Optional.of(new ContractModel(CONTRACT_UUID, "password", "test", true, 1L)));
        when(sessionRepository.save(any())).then(invocation -> invocation.getArgument(0));

        Session session = sessionService.createSession(request, now);
        assertThat(session.getExpires(), is(now.plus(SessionService.SESSION_LENGTH)));
    }

    @Test
    void extendSession() {
        LocalDateTime now = LocalDateTime.of(2020, 6, 1, 12, 0, 0);
        UUID SESSION_ID = UUID.fromString("00000000-4176-4c73-b121-8d8fc13a4400");

        when(sessionRepository.findById(SESSION_ID))
                .thenReturn(Optional.of(new SessionModel(SESSION_ID, null, null, now, 1L)));
        when(sessionRepository.save(any())).then(invocation -> invocation.getArgument(0));

        Session session = sessionService.extendSession(SESSION_ID, now);
        assertThat(session.getExpires(), is(now.plus(SessionService.SESSION_LENGTH)));
    }
}