
create table public.contract
(
    id uuid not null primary key,
    password varchar(50) not null,
    name varchar(50) not null,
    internal boolean not null,
    version bigint
);

insert into public.contract(id, password, name, internal) VALUES
('f36e4d14-4a1a-458d-9085-ff8a313f5bea', 'BSD8aGA3BmUAx2WrpTyFLmoVQxBkxInI', 'Admin', true),
('a0d4e415-a0cc-4a9f-85a5-a69e99370862', 'Hgkz6qQqYI73lCXfJ6pjFjb7HNlIGURA', 'AlqGateway', true),
('395b9874-6e17-4953-92c8-c369cc93c1a2', '8IqZKrdJLbzovPDNmXfPoYOMOhlIFIW5', 'AlqSession', true),
('095e58e0-6cc8-4050-a200-25e0a6768d57', 'F0QNk0KuAUENz8mGEJRObUUCs0PoOKfO', 'AlqConfig', true),
('a3994e73-23aa-4046-a01b-fe5917e479bb', '', 'AlqExternal', false);

create table public.session
(
--     session_id uuid default gen_random_uuid() not null primary key,
    session_id uuid not null primary key,
    contract uuid not null,
    tenant uuid not null,
    expires timestamp not null,
    version bigint
);

