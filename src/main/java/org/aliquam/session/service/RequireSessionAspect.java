package org.aliquam.session.service;

import org.aliquam.session.api.AlqSessionApi;
import org.aliquam.session.api.RequestAuthenticationException;
import org.aliquam.session.api.RequestAuthenticatorService;
import org.aliquam.session.api.model.Session;
import org.aliquam.session.service.mapper.SessionMapper;
import org.aliquam.session.service.repository.SessionRepository;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Optional;
import java.util.UUID;

@Aspect
@Component
public class RequireSessionAspect {

    private final SessionRepository sessionRepository;
    private final SessionMapper sessionMapper;
    private final RequestAuthenticatorService requestAuthenticatorService;

    public RequireSessionAspect(SessionRepository sessionRepository, SessionMapper sessionMapper) {
        this.sessionRepository = sessionRepository;
        this.sessionMapper = sessionMapper;
        requestAuthenticatorService = new RequestAuthenticatorService(this::getSessionById);
    }

    private Optional<Session> getSessionById(UUID id) {
        return sessionRepository.findById(id)
                .map(sessionMapper::map);
    }

    @Around("@annotation(org.aliquam.session.service.RequireSession)")
    public Object handleRequireSession(ProceedingJoinPoint joinPoint) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        RequireSession requireSession = method.getAnnotation(RequireSession.class);

        String sessionHeader = request.getHeader(AlqSessionApi.SESSION_HEADER);
        try {
            requestAuthenticatorService.checkSession(sessionHeader, requireSession.internal());
        } catch (RequestAuthenticationException e) {
            throw new ResponseStatusException(e.getErrorHttpStatus(), e.getMessage(), e);
        }

        return joinPoint.proceed();
    }

}