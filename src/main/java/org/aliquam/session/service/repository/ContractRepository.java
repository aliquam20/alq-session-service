package org.aliquam.session.service.repository;

import org.aliquam.session.service.model.ContractModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ContractRepository extends CrudRepository<ContractModel, UUID> {
}
