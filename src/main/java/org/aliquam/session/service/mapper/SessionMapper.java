package org.aliquam.session.service.mapper;

import org.aliquam.session.api.model.Session;
import org.aliquam.session.service.model.SessionModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = ContractMapper.class)
public interface SessionMapper {
    Session map(SessionModel sessionModel);

    @Mapping(target = "version", ignore = true)
    SessionModel map(Session session);
}
