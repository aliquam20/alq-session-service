package org.aliquam.session.service.mapper;

import org.aliquam.session.api.model.Contract;
import org.aliquam.session.api.model.CreateContractRequest;
import org.aliquam.session.service.model.ContractModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper
public interface ContractMapper {
    Contract map(ContractModel contractModel);

    @Mapping(target = "password", ignore = true)
    @Mapping(target = "version", ignore = true)
    ContractModel map(Contract contract);

    @Mapping(target = "version", ignore = true)
    ContractModel map(CreateContractRequest request);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "version", ignore = true)
    ContractModel map(CreateContractRequest request, @MappingTarget ContractModel contractModel);
}
