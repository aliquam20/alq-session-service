package org.aliquam.session.service.service;

import lombok.extern.slf4j.Slf4j;
import org.aliquam.session.api.model.Contract;
import org.aliquam.session.api.model.CreateContractRequest;
import org.aliquam.session.service.mapper.ContractMapper;
import org.aliquam.session.service.model.ContractModel;
import org.aliquam.session.service.repository.ContractRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@Slf4j
public class ContractService {
    private final ContractRepository contractRepository;
    private final ContractMapper contractMapper;

    public ContractService(ContractRepository contractRepository, ContractMapper contractMapper) {
        this.contractRepository = contractRepository;
        this.contractMapper = contractMapper;
    }

    @Transactional
    public Contract putContract(@RequestBody @Valid CreateContractRequest request) {
        ContractModel contractModel = contractRepository.findById(request.getId())
                .map(existing -> contractMapper.map(request, existing))
                .orElseGet(() -> contractMapper.map(request));

        contractRepository.save(contractModel);
        return contractMapper.map(contractModel);
    }

    public List<Contract> listContracts() {
        Iterable<ContractModel> contractModels = contractRepository.findAll();
        return StreamSupport.stream(contractModels.spliterator(), false)
                .map(contractMapper::map)
                .collect(Collectors.toList());
    }

    @Transactional
    public void deleteContract(@PathVariable @NotNull UUID id) {
        if (contractRepository.existsById(id)) {
            contractRepository.deleteById(id);
        }
    }

}
