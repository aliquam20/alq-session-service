package org.aliquam.session.service.service;

import lombok.extern.slf4j.Slf4j;
import org.aliquam.session.api.model.CreateSessionRequest;
import org.aliquam.session.api.model.Session;
import org.aliquam.session.service.mapper.SessionMapper;
import org.aliquam.session.service.model.ContractModel;
import org.aliquam.session.service.model.SessionModel;
import org.aliquam.session.service.repository.ContractRepository;
import org.aliquam.session.service.repository.SessionRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.UUID;

@Service
@Slf4j
public class SessionService {
    public static final Duration SESSION_LENGTH = Duration.ofDays(10);

    private final SessionRepository sessionRepository;
    private final ContractRepository contractRepository;
    private final SessionMapper sessionMapper;

    public SessionService(SessionRepository sessionRepository, ContractRepository contractRepository, SessionMapper sessionMapper) {
        this.sessionRepository = sessionRepository;
        this.contractRepository = contractRepository;
        this.sessionMapper = sessionMapper;
    }

    @Transactional
    public Session createSession(@RequestBody @Valid CreateSessionRequest request, LocalDateTime now) {
        log.info("Received createSession request: {}", request);
        ContractModel contractModel = contractRepository.findById(request.getContract())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Contract not found"));

        if(!contractModel.getPassword().equals(request.getPassword())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Invalid password");
        }
        if(!contractModel.isInternal() && request.getTenant() == null) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Missing tenant");
        }

        UUID uuid = UUID.randomUUID();
        LocalDateTime expires = now.plus(SESSION_LENGTH);
        SessionModel sessionModel = new SessionModel(uuid, contractModel, request.getTenant(), expires, 1L);
        sessionModel = sessionRepository.save(sessionModel);
        log.info("Session created: {}", sessionModel.getId());
        return sessionMapper.map(sessionModel);
    }

    public Session readSession(@PathVariable @NotNull UUID id) {
        SessionModel sessionModel = sessionRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Session does not exist"));
        return sessionMapper.map(sessionModel);
    }

    @Transactional
    public Session extendSession(@PathVariable @NotNull UUID id, LocalDateTime now) {
        SessionModel sessionModel = sessionRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Session does not exist"));
        LocalDateTime expires = now.plus(SESSION_LENGTH);
        sessionModel.setExpires(expires);
        sessionRepository.save(sessionModel);
        return sessionMapper.map(sessionModel);
    }

}
