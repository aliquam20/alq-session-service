package org.aliquam.session.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
public class AlqSessionServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlqSessionServiceApplication.class, args);
	}

}
