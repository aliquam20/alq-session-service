package org.aliquam.session.service.controller;

import org.aliquam.session.api.model.Contract;
import org.aliquam.session.api.model.CreateContractRequest;
import org.aliquam.session.service.RequireSession;
import org.aliquam.session.service.service.ContractService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@RestController
public class ContractController {

    private final ContractService contractService;

    public ContractController(ContractService contractService) {
        this.contractService = contractService;
    }

    @PutMapping("/contract")
    @RequireSession(internal = true)
    public Contract putContract(@RequestBody @Valid CreateContractRequest request) {
        return contractService.putContract(request);
    }

    @GetMapping("/contract")
    @RequireSession(internal = true)
    public List<Contract> listContracts() {
        return contractService.listContracts();
    }

    @DeleteMapping("/contract/{id}")
    @RequireSession(internal = true)
    public void deleteContract(@PathVariable @NotNull UUID id) {
        contractService.deleteContract(id);
    }

}
