package org.aliquam.session.service.controller;

import lombok.extern.slf4j.Slf4j;
import org.aliquam.session.api.model.CreateSessionRequest;
import org.aliquam.session.api.model.Session;
import org.aliquam.session.service.RequireSession;
import org.aliquam.session.service.service.SessionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@RestController
@Slf4j
public class SessionController {
    private final SessionService sessionService;

    public SessionController(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @PostMapping("/session")
    public Session createSession(@RequestBody @Valid CreateSessionRequest request) {
        return sessionService.createSession(request, LocalDateTime.now());
    }

    @GetMapping("/session/{id}")
    @RequireSession(internal = true)
    public Session readSession(@PathVariable @NotNull UUID id) {
        return sessionService.readSession(id);
    }

    @PostMapping("/session/{id}/extend")
    public Session extendSession(@PathVariable @NotNull UUID id) {
        return sessionService.extendSession(id, LocalDateTime.now());
    }

}
