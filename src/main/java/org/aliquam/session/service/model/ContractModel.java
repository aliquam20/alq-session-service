package org.aliquam.session.service.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import java.util.UUID;

@Entity
@Table(name = "contract")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContractModel {

    @Id
    private UUID id;

    private String password;

    private String name;

    private boolean internal;

    @Version
    private Long version;
}
