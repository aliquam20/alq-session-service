
create table public.contract
(
    id uuid not null primary key,
    password varchar(50) not null,
    name varchar(50) not null,
    internal boolean not null,
    version bigint
);

insert into public.contract("id", "password", "name", "internal") VALUES
('f36e4d14-4a1a-458d-9085-ff8a313f5bea', 'BSD8aGA3BmUAx2WrpTyFLmoVQxBkxInI', 'Admin', true),
('a0d4e415-a0cc-4a9f-85a5-a69e99370862', 'Hgkz6qQqYI73lCXfJ6pjFjb7HNlIGURA', 'AlqGateway', true),
('395b9874-6e17-4953-92c8-c369cc93c1a2', '8IqZKrdJLbzovPDNmXfPoYOMOhlIFIW5', 'AlqSession', true),
('095e58e0-6cc8-4050-a200-25e0a6768d57', 'F0QNk0KuAUENz8mGEJRObUUCs0PoOKfO', 'AlqConfig', true),
('9534a876-ae73-4de6-9408-efb842e9f83b', 'wm64PjPSg6Vhrlx6ThHlH1BiL20HJpf7', 'AlqPerms', true),
('b7f4f246-a369-4481-b8d1-772ac5a59311', 'R25ABb50RS2NxLXwE3DPs2jwtJ1mkMx9', 'AlqPlayers', true),
('c7c11134-584f-4d97-8c21-9605aa0d1922', 'NUMOpLqypaNvO5vCdAQ2b1yIQbmXqwGw', 'AlqWorlds', true),
('5deb11cd-90bc-4b57-a0a9-f8e47c0479d8', 'eyVEB5Sl6Zq5XuFl7tKoKT3Gd4celRwd', 'AlqGenerator', true),
('8d567ba7-682c-440d-a135-c7ee3d2ac651', 's95jDcXFDtwO3St1sBkJFjZttUBQrseS', 'AlqChat', true),
('a3994e73-23aa-4046-a01b-fe5917e479bb', 'ItVX73uS5VSnjPdE0dRqh0lmeQo8BxHl', 'AlqExternal', false);

create table public.session
(
    id uuid not null primary key,
    contract uuid not null,
    tenant uuid,
    expires timestamp not null,
    version bigint
);

ALTER TABLE public.session
    ADD CONSTRAINT FK_SESSION_ON_CONTRACT FOREIGN KEY (contract) REFERENCES contract (id)
    ON DELETE CASCADE;

